package caronae.com.scan.library.demo;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.scanlibrary.ScanActivity;
import com.scanlibrary.ScanConstants;

import java.io.ByteArrayOutputStream;
import java.io.IOException;

import static com.scanlibrary.Utils.REQUEST_CODE;

public class MainActivity extends AppCompatActivity {

    private Button btn_start;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        /*// full screen activity
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);*/

        btn_start = (Button) findViewById(R.id.btn_start);

        btn_start.setOnClickListener(new ScanClickListener());

    }

    public class ScanClickListener implements View.OnClickListener{

        @Override
        public void onClick(View view) {
            // create new Intent to start ScanActivity
            Intent intent = new Intent(MainActivity.this, ScanActivity.class);
/*
            // send the images
            // Logo header, height: 60dp
            Bitmap bitmap_logo_header = BitmapFactory.decodeResource(getResources(), R.drawable.logo_cih);
            // Capture image, size : 60 dp
            Bitmap bitmap_btn_capture_image = BitmapFactory.decodeResource(getResources(), R.drawable.ic_camera_icon_60dp);
            // Load file image, size : 36 dp
            Bitmap bitmap_btn_load_file = BitmapFactory.decodeResource(getResources(), R.drawable.ic_load_file_36dp);
            Bitmap bitmap_ic_launcher = BitmapFactory.decodeResource(getResources(), R.drawable.ic_image_capture_60dp);
            byte[] baos_launcher = getBaos(bitmap_ic_launcher);


            // **** Transform resources to  byte Array Output Stream ****
            // logo header
            byte[] baos_logo_header = getBaos(bitmap_logo_header);
            // image for capture picture button
            byte[] baos_btn_capture_image = getBaos(bitmap_btn_capture_image);
            // image for load file button
            byte[] baos_btn_load_file = getBaos(bitmap_btn_load_file);

            // hexa code for the color of buttons
            String st_btn_color = "#6bf442";
            // text for scan button
            String st_scan_button = "Scan";
            // text for finish button
            String st_finish_button = "Finish";
            // text for retry button
            String st_retry = "Retry";

            // **** sending variables to Scan activity, they have to match their IDs ****
            // img capture image
            intent.putExtra(Utils.IMG_BTN_CAPTURE, baos_btn_capture_image);
            // img flash button
            intent.putExtra(Utils.IMG_BTN_FLASH, baos_launcher);
            // string flash button
            intent.putExtra(Utils.STRING_CONTOUR_COLOR, "#6bf442");
            // img back button
            intent.putExtra(Utils.IMG_BACK_BUTTON, baos_launcher);
            // string back button
            intent.putExtra(Utils.STRING_BACK_BUTTON, "Back");
            // img rotate left button
            intent.putExtra(Utils.IMG_ROTATE_LEFT, baos_launcher);
            // img rotate right button
            intent.putExtra(Utils.IMG_ROTATE_RIGHT, baos_launcher);
            // img load file from storage
            intent.putExtra(Utils.IMG_LOAD_FILE, baos_btn_load_file);
            // img logo header
            intent.putExtra(Utils.IMG_LOGO_HEADER, baos_logo_header);
            // img button color
            intent.putExtra(Utils.BUTTON_COLOR, st_btn_color);
            // string scan button
            intent.putExtra(Utils.STRING_SCAN, st_scan_button);
            // string finish button
            intent.putExtra(Utils.STRING_FINISH, st_finish_button);
            // string retry button
            intent.putExtra(Utils.STRING_RETRY, st_retry);*/

            // **** start the scan activity ****
            startActivityForResult(intent, REQUEST_CODE);
        }
    }

    /**
     * get byte Array Output Stream
     * @param bitmap
     * @return
     */
    public byte[] getBaos(Bitmap bitmap){
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG, 100, baos);
        return baos.toByteArray();
    }


    //Once the scanning is done, the application is returned from scan library to main app, to retrieve the scanned image,
    // add onActivityResult in your activity or fragment from where you have started startActivityForResult,
    // below is the sample code snippet:
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_CODE && resultCode == Activity.RESULT_OK) {
            // here we get the unified resource dientifier of the scanned image
            Uri uri = data.getExtras().getParcelable(ScanConstants.SCANNED_RESULT);
            Bitmap bitmap = null;
            try {
                // here we get our scanned bitmap
                bitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), uri);
                getContentResolver().delete(uri, null, null);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
